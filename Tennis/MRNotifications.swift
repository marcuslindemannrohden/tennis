//
//  MRNotifications.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/27/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit

class MRNotifications: NSObject {
    static func registerForNotifications()
    {
        let settings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
}
