//
//  MRTournamentMapViewController.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/14/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit
import MapKit

class MRTournamentMapViewController: UIViewController {
    
    var locationManager: CLLocationManager!
    @IBOutlet var mapViewTournament: MKMapView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        mapViewTournament.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }

    
    override func viewWillAppear(animated: Bool) {
        requestRoute()
        placePointsAnnotations()
    }
    
    override func viewDidAppear(animated: Bool) {
        centerBetweenAnnotations()
    }
    
    
    func centerBetweenAnnotations()
    {
        var zoomRect = MKMapRectNull
        for each in mapViewTournament.annotations
        {
            let annotationPoint = MKMapPointForCoordinate(each.coordinate)
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
            if(MKMapRectIsNull(zoomRect))
            {
                zoomRect = pointRect
            }else{
                zoomRect = MKMapRectUnion(zoomRect, pointRect)
            }
        }
        let edgePadding = UIEdgeInsetsMake(200, 200, 200, 200)
        mapViewTournament.setVisibleMapRect(zoomRect, edgePadding: edgePadding, animated: true)
    }
    
    
    func findEventLocation()
    {
        if(CLLocationManager.locationServicesEnabled())
        {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            setMapPositions(MRTournament.sharedInstance.placePointCLLocation.coordinate)
        }
    }
    
    func findActualLocation()
    {
        if(CLLocationManager.locationServicesEnabled())
        {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func placePointsAnnotations()
    {
        let annotation = MKPointAnnotation()
        annotation.coordinate = MRTournament.sharedInstance.placePointCLLocation.coordinate
        annotation.title = MRTournament.sharedInstance.nameString
        mapViewTournament.addAnnotation(annotation)
    }
    
    
    
    
    func setMapPositions(coords: CLLocationCoordinate2D)
    {
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let lat = coords.latitude
        let lng = coords.longitude
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        
        
        let region = MKCoordinateRegion(center: location, span: span)
        mapViewTournament.setRegion(region, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
   
    @IBAction func tapOnCurrentLocation(sender: UIButton)
    {
        findActualLocation()
    }
    
    @IBAction func tapOnEventLocation(sender: UIButton) {
        findEventLocation()
    }
    
    @IBAction func tapOnResetLocation(sender: AnyObject) {
        centerBetweenAnnotations()
    }
    
    func requestRoute()
    {

        let request = MKDirectionsRequest()
        request.source = MKMapItem.mapItemForCurrentLocation()
        let destPlacemark = MKPlacemark(coordinate: MRTournament.sharedInstance.placePointCLLocation.coordinate, addressDictionary: nil)
        request.destination = MKMapItem(placemark: destPlacemark)
        let directions = MKDirections(request: request)
        directions.calculateDirectionsWithCompletionHandler { (directionsResponse, error) -> Void in
            if(error == nil)
            {
                self.showRoute(directionsResponse!)
            }else{
                self.showAlertWithSingleButton("Error", message: "Error code: \(error?.code)")
            }
        }
    }
    
    
    func showRoute(directionsResponse: MKDirectionsResponse)
    {
        for each in directionsResponse.routes as [MKRoute] {
            mapViewTournament.addOverlay(each.polyline, level: MKOverlayLevel.AboveRoads)
        }
    }
}

extension MRTournamentMapViewController: MKMapViewDelegate
{
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = MRTournament.sharedInstance.nameString
        let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        
        let tournamentLocation = MRTournament.sharedInstance.placePointCLLocation.coordinate
        
        if(annotation.coordinate.latitude == tournamentLocation.latitude && annotation.coordinate.longitude == tournamentLocation.longitude){
            pinView.animatesDrop = true
            pinView.canShowCallout = true
            pinView.pinColor = MKPinAnnotationColor.Purple
            return pinView
        }else{
            return nil
        }
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer
    {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.purpleColor()
            renderer.lineWidth = 5.0
            return renderer
    }
}


extension MRTournamentMapViewController: CLLocationManagerDelegate
{
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        setMapPositions((manager.location?.coordinate)!)
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error while updating location " + error.localizedDescription)
    }
}
