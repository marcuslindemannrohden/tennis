//
//  MRTournamentTableViewCell.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/9/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit

class MRTournamentTableViewCell: UITableViewCell {
    @IBOutlet var imageViewCellFlyer: UIImageView!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelHour: UILabel!
    @IBOutlet var labelPlace: UILabel!
    @IBOutlet var labelName: UILabel!

    @IBOutlet var activIndicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
