//
//  AppDelegate.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/9/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit
import Parse
import Bolts

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        Parse.enableLocalDatastore()
        Parse.setApplicationId("L8lEONvbndeI5sW3hcYbLetXcejoRB0LjYJtQiI8", clientKey: "wdtSOQkXf5Mnh9BewRh2SDUpQF7aHCdM9zzBSLF7")
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        MRNotifications.registerForNotifications()
        
//        27 133 86 100 - verde
//        98 27 133 100 - roxo
//        226 224 217 100 - areia
//        27 105 133 100 - azul
//        133 89 27 100 - marrom
        
        
        let navBar = UINavigationBar.appearance()
        navBar.tintColor = UIColor.whiteColor()
//        navBar.tintColor = UIColor(red: 27/255, green: 133/255, blue: 86/255, alpha: 1) //- verde
//        navBar.tintColor = UIColor(red: 98/255, green: 27/255, blue: 133/255, alpha: 1) // - roxo
//        navBar.tintColor = UIColor(red: 226/255, green: 224/255, blue: 217/255, alpha: 1) // - areia
//        navBar.tintColor = UIColor(red: 27/255, green: 105/255, blue: 133/255, alpha: 1) // - azul
//        navBar.tintColor = UIColor(red: 133/255, green: 89/255, blue: 27/255, alpha: 1) // - marrom
        
        navBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        navBar.setBackgroundImage(UIImage(named: "navbar-2"), forBarMetrics: UIBarMetrics.Default)
        navBar.translucent = true
        
        return true
    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
    {
        let currentInstallation = PFInstallation.currentInstallation()
        currentInstallation.setDeviceTokenFromData(deviceToken)
        currentInstallation.channels = ["tennis"]
        currentInstallation.saveEventually { (success, error) -> Void in
            if(success){
                print("PUSHNOTIFICATIONS OK")
            }else{
                print(error)
            }
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError)
    {
        print("Couldn't register: \(error)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("-----------------------------------PUSHCHEGOU!-----------------\(NSDate())------------------")
        print(userInfo)
//        NSNotificationCenter.defaultCenter().postNotificationName("updateProductsList", object: userInfo)
        
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

