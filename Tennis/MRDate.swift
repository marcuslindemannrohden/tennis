//
//  MRDate.swift
//  MRKit
//
//  Created by Marcus Lindemann Rohden on 10/9/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import Foundation
import UIKit

extension NSDate
{
    func selfIsGreaterThanDate(dateToCompare : NSDate) -> Bool
    {
        //Return Result
        return self.compare(dateToCompare) == NSComparisonResult.OrderedDescending
    }
    
    
    func selfIsLessThanDate(dateToCompare : NSDate) -> Bool
    {
        //Return Result
        return self.compare(dateToCompare) == NSComparisonResult.OrderedAscending
    }
    
    
    func selfIsEqualToDate(dateToCompare : NSDate) -> Bool
    {
        //Return Result
        return self.compare(dateToCompare) == NSComparisonResult.OrderedSame
    }
    
    
    func selfAddDays(daysToAdd : Int) -> NSDate
    {
        let secondsInDays : NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded : NSDate = self.dateByAddingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    
    func selfAddHours(hoursToAdd : Int) -> NSDate
    {
        let secondsInHours : NSTimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded : NSDate = self.dateByAddingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    
    func selfAddMinutes(minutesToAdd : Int) -> NSDate
    {
        let secondsInMinutes : NSTimeInterval = Double(minutesToAdd) * 60
        let dateWithHoursAdded : NSDate = self.dateByAddingTimeInterval(secondsInMinutes)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    
    func selfAddSeconds(secondsToAdd : Int) -> NSDate
    {
        let seconds : NSTimeInterval = Double(secondsToAdd)
        let dateWithHoursAdded : NSDate = self.dateByAddingTimeInterval(seconds)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func formatDate(dateToParse: NSDate, dateFormat: String) -> String{
        let formatter = NSDateFormatter()
        formatter.dateFormat = dateFormat
        return formatter.stringFromDate(dateToParse)
    }
    
    func setDateToMidnight() -> NSDate
    {
        let date = self
        let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        
        // Swift 2:
        let components = cal.components([.Day , .Month, .Year ], fromDate: date)
        
        let newDate = cal.dateFromComponents(components)
        return newDate!
    }
    
}
