//
//  MRTournamentFullImageViewController.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/27/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit

class MRTournamentFullImageViewController: UIViewController {
    @IBOutlet var imageViewfullFlyer: UIImageView!

    @IBOutlet var imageViewFullBg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        MRAppearence.setupBG(self.view)
        
        navigationItem.title = MRTournament.sharedInstance.nameString
        imageViewfullFlyer.image = MRTournament.sharedInstance.flyerFullImage
        
        
        
        
        imageViewFullBg.image = MRTournament.sharedInstance.flyerFullImage
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        imageViewFullBg.addSubview(blurView)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapCloseButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
