//
//  MRTournamentDetailsViewController.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/12/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit

protocol MRTournamentDetailsDelegate
{
    func alreadyLoadedImage(imageSetted: UIImage)
}

class MRTournamentDetailsTableViewController: UITableViewController {
    
    @IBOutlet var labelStartDate: UILabel!
    @IBOutlet var labelEndDate: UILabel!
    @IBOutlet var labelPlace: UILabel!
    @IBOutlet var labelTournamentDescription: UILabel!
    @IBOutlet var imageViewFullFlyer: UIImageView!
    @IBOutlet var activIndicator: UIActivityIndicatorView!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MRAppearence.setupBG(self.view)
        
        navigationItem.title = MRTournament.sharedInstance.nameString
        setAllFieldsFromCurrentEvent()
        
        loadFullFlyerImageAsync()
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        imageViewFullFlyer.layer.borderColor = UIColor.whiteColor().CGColor
        imageViewFullFlyer.layer.borderWidth = 2
        imageViewFullFlyer.layer.cornerRadius = 5
        imageViewFullFlyer.layer.masksToBounds = true
    }
    
    
    @IBAction func tapOnBackBarItem(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func tapOnMapBarItem(sender: AnyObject) {
        self.performSegueWithIdentifier("showTournamentMap", sender: nil)
    }
    
    func loadFullFlyerImageAsync()
    {
        if let coverAux: UIImage = MRTournament.sharedInstance.flyerFullImage
        {
            imageViewFullFlyer.image = coverAux
            activIndicator.stopAnimating()
        }else{
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0), { () -> Void in
                ImageLoader.sharedLoader.imageForUrl(MRTournament.sharedInstance.flyerFullString, completionHandler: { (image, url, data) -> () in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.imageViewFullFlyer.image = image
                        MRTournament.sharedInstance.flyerFullImage = image
                        self.activIndicator.stopAnimating()
                        self.tableView.reloadData()
                    })
                })
            })
        }
    }
    
    func setAllFieldsFromCurrentEvent()
    {
        let stDate = NSDate().formatDate(MRTournament.sharedInstance.startDate, dateFormat: "dd/MM/YY")
        let edDate = NSDate().formatDate(MRTournament.sharedInstance.endDate, dateFormat: "dd/MM/YY")
        labelStartDate.text = "Data de Inicio: \(stDate) - \(MRTournament.sharedInstance.hours)"
        labelEndDate.text = "Data de Encerramento: \(edDate)"
        labelPlace.text = MRTournament.sharedInstance.placeString
        labelTournamentDescription.text = MRTournament.sharedInstance.descriptionString
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row == 2)
        {
            self.performSegueWithIdentifier("showTournamentImage", sender: nil)
        }
    }
    
    
    
}
