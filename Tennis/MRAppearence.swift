//
//  MRAppearence.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/28/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit

class MRAppearence: NSObject {
    static func setupBG(viewToAddBG: UIView)
    {
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let screenHeight = UIScreen.mainScreen().bounds.size.height * 2
        let imageView = UIImageView(frame: CGRectMake(0, 0 - screenHeight / 4 , screenWidth , screenHeight));
        let image = UIImage(named: "BG-2");
        imageView.image = image;
        viewToAddBG.insertSubview(imageView, atIndex: 0)
    }
}
