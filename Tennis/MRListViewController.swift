//
//  ViewController.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/9/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit
import Parse

class MRListViewController: UIViewController {

    var convertedListOfTournaments: [MRTournament]!
    
    @IBOutlet var tableViewTournaments: UITableView!
    @IBOutlet var activIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        convertedListOfTournaments = []
        
        populateDataByDate { () -> () in
            self.activIndicator.stopAnimating()
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateDataByDate(completionHandler: () -> ())
    {
        let date = NSDate().setDateToMidnight()
        MRDAOTournament.loadAllTournamentsBeetwenDate(date) { (listOfTournaments, error) -> () in
            if(error == nil)
            {
                for each in listOfTournaments
                {
                    if(date.selfIsLessThanDate(each.endDate) || date.selfIsEqualToDate(each.endDate))
                    {
                        self.convertedListOfTournaments.append(each)
                    }
                }
                if(self.convertedListOfTournaments.count > 0){
                    self.tableViewTournaments.reloadData()
                }
            }else{
              self.showAlertWithSingleButton("Error", message: "Error code \(error?.code)")
            }
            completionHandler()
        }
    }
}

extension MRListViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return convertedListOfTournaments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MRTournamentTableViewCell
        let tournament = convertedListOfTournaments[indexPath.row]

        cell.imageViewCellFlyer.image = nil
        
        cell.labelName.text = tournament.nameString
        cell.labelHour.text = tournament.hours
        let tournamentDate: String = NSDate().formatDate(tournament.startDate, dateFormat: "dd/MM/YY")
        cell.labelDate.text = tournamentDate
        cell.labelPlace.text = tournament.placeString
        
        
        if let coverAux: UIImage = tournament.flyerCellImage
        {
            cell.imageViewCellFlyer.image = coverAux
            cell.activIndicator.stopAnimating()
        }else{
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0), { () -> Void in
                ImageLoader.sharedLoader.imageForUrl(tournament.flyerCellString, completionHandler: { (image, url, data) -> () in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        cell.imageViewCellFlyer.image = image
                        self.convertedListOfTournaments[indexPath.row].flyerCellImage = image
                        cell.activIndicator.stopAnimating()
                    })
                })
            })
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        MRTournament.sharedInstance.populateWithData(convertedListOfTournaments[indexPath.row].parseTournamentPFObject)
        self.performSegueWithIdentifier("showTournamentDetails", sender: indexPath)
    }
}

