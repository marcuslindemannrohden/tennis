//
//  MRDAOTournament.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/9/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit
import Parse

class MRDAOTournament: NSObject {
    
    static func loadAllTournamentsBeetwenDate(dateToCheck: NSDate, completionHandler: (listOfTournaments: [MRTournament], error: NSError?) -> ())
    {
        let query = PFQuery(className: "Tournament")
        var resultList: [MRTournament] = []
        query.whereKey("start_date", greaterThanOrEqualTo: dateToCheck)
        query.findObjectsInBackgroundWithBlock { (listOfPFObjects, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if(error == nil)
                {
                    for each in listOfPFObjects!
                    {
                        let tournament = MRTournament()
                        tournament.populateWithData(each)
                        resultList.append(tournament)
                    }
                }
                completionHandler(listOfTournaments: resultList, error: error)
            })
            
        }
        
    
        
        
    }
}
