//
//  MRAlert.swift
//  MRKit
//
//  Created by Marcus Lindemann Rohden on 10/9/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController
{
    func showAlertWithSingleButton(title: String, message: String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlertViewWithTwoOptions(titleOfAlert: String, messageOfAlert: String, firstButtonTitle: String, secondButtonTitle: String, completionHandler: (clickedOnButtonIndex: NSInteger, action: UIAlertAction)->())
    {
        let alert = UIAlertController(title: titleOfAlert, message: messageOfAlert, preferredStyle: UIAlertControllerStyle.Alert)
        
        let firstButton = UIAlertAction(title: firstButtonTitle, style: UIAlertActionStyle.Default) { (firstAction) -> Void in
            completionHandler(clickedOnButtonIndex: 0, action: firstAction)
        }
        let secondButton = UIAlertAction(title: secondButtonTitle, style: UIAlertActionStyle.Default) { (secondAction) -> Void in
            completionHandler(clickedOnButtonIndex: 1, action: secondAction)
        }
        
        alert.addAction(firstButton)
        alert.addAction(secondButton)
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
}