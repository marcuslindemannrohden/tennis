//
//  MRTournament.swift
//  Tennis
//
//  Created by Marcus Lindemann Rohden on 10/9/15.
//  Copyright © 2015 MarcusRohden. All rights reserved.
//

import UIKit
import Parse

class MRTournament: NSObject {
    var nameString: String!
    var descriptionString: String!
    var startDate: NSDate!
    var endDate: NSDate!
    var flyerCellString: String!
    var flyerFullString: String!
    var flyerCellImage: UIImage!
    var flyerFullImage: UIImage!
    
    var hours: String!
    var placeString: String!
    var placePointCLLocation: CLLocation!
    
    var parseObjectId: String!
    var parseTournamentPFObject: PFObject!
    
    static let sharedInstance = MRTournament()
    
    override init()
    {
        parseTournamentPFObject = PFObject(className: "Tournament")
    }
    
    func populateWithData(parseObjectToSet: PFObject)
    {
        if let name = parseObjectToSet["name"]
        {
            nameString = name as! String
        }
        
        if let desc = parseObjectToSet["description"]
        {
            descriptionString = desc as! String
        }
        
        if let stDate = parseObjectToSet["start_date"]
        {
            startDate = stDate as! NSDate
        }
        
        if let edDate = parseObjectToSet["end_date"]
        {
            endDate = edDate as! NSDate
        }
        
        if let flyCell: PFFile = parseObjectToSet["flyer_cell"] as? PFFile
        {
            flyerCellString = flyCell.url
        }
        
        if let flyFull: PFFile = parseObjectToSet["flyer_full"] as? PFFile
        {
            flyerFullString = flyFull.url
        }
        
        if let hr = parseObjectToSet["hour"]
        {
            hours = hr as! String
        }
        
        if let plc = parseObjectToSet["place"]
        {
            placeString = plc as! String
        }
        
        if let pnt: PFGeoPoint = parseObjectToSet["place_point"] as? PFGeoPoint
        {
            placePointCLLocation = CLLocation(latitude: pnt.latitude, longitude: pnt.longitude)
        }
        
        if let obId = parseObjectToSet.objectId
        {
            parseObjectId = obId
        }
        
        parseTournamentPFObject = parseObjectToSet
    }
    
}
